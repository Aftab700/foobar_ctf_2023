const express = require('express')
var cookieSession = require('cookie-session')
const fileUpload = require('express-fileupload');
const AdmZip = require("adm-zip");
var jwt = require('jsonwebtoken');
var cookieParser = require('cookie-parser')
const fs = require('fs');
const libxmljs = require("libxmljs");
const app = express()


// set the view engine to ejs
app.set('view engine', 'ejs');
app.use(cookieSession({ keys: ['haxor1337'] }));
app.use(cookieParser())
app.use('/public/', express.static('./public'));
app.use(express.urlencoded({ extended: false }))
app.use(fileUpload());

var privateKey = fs.readFileSync('./keys/private.key', 'utf8');
var publicKey = fs.readFileSync('./keys/public.key', 'utf8');
var filepath = './public/images/privkey.txt'

app.get('/robots.txt', function (req, res) {
    res.type('text/plain');
    res.send("Are you looking for this ?\n/privkey\n/app/flag.txt");
});

app.get('/', function (req, res) {
    res.render('index.ejs')
})

app.get('/privkey', function (req, res) {
    res.sendFile(filepath, { root: __dirname });
})

app.post('/login', function (req, res) {
    var username = req.body.username
    var password = req.body.password
    if (username && password) {
        var token = jwt.sign({ role: 'rookie' }, privateKey, { algorithm: 'RS256' });
        res.cookie('token', token, { maxAge: 900000, httpOnly: true });
        console.log(token);
        res.redirect(302, '/apply')

    }
    else {
         res.redirect(302, '/')
    }
})

app.get('/apply', function (req, res) {
    var cookie = req.cookies;
    jwt.verify(cookie['token'], publicKey, function (err, auth) {
        if (err) {
            console.log(err)
            res.sendStatus(403);
        } else {
            console.log(auth);
            var decoded = jwt.decode(cookie['token'])
            if (decoded['role'] == 'hacker') {
                res.render('apply.ejs', { info: req.session.info })
            } else {
                res.render('reject.ejs')
            }

        }
    })
})

app.post('/upload', function (req, res) {
    var info = {}
    let err_msg = '';
    if (!req.files || Object.keys(req.files).length === 0) {
        return res.status(400).send('No files were uploaded.');
    }

    let f = req.files.file;
    if (req.files.file.size < 2000000) {
        let uploadPath = __dirname + '/uploads/' + f.md5;
        f.mv(uploadPath, function (err) {
            if (err) {
                console.log('[+] fucked up')
                return res.status(500).send(err);

            } else {

                try {
                    var zip = new AdmZip(uploadPath);
                    var zipEntries = zip.getEntries();
                    zipEntries.forEach(function (zipEntry) {
                        if (zipEntry.entryName == "docProps/app.xml") {
                            var d = zipEntry.getData().toString("utf8")
                            var docXML = libxmljs.parseXml(d, { noent: true, noblanks: true })
                            let root = docXML.root().namespace().href();
                            let txt = docXML.get('xmlns:Words', root)
                            info.words = txt.text()
                        }
                    });
                } catch (err) {
                    err_msg = "Some error occured , plz use docx file only...";
                    return res.render('apply', { err_msg: err_msg , info: req.session.info});
                    
                }
                if (!info.words) {
                    info.words = "0"
                }
                req.session.info = info;
                res.redirect('/apply')

            }
        });
    } else {
        err_msg = "File size exceeded...";
        return res.render('apply', { err_msg: err_msg, info: req.session.info} );
        
    }
});

app.listen(9999, () => console.log('[+] Server Started'));