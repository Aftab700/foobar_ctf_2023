const express = require('express');
const bodyparser = require('body-parser');
const { graphqlHTTP } = require('express-graphql');
const mongoose = require('mongoose')
const bcrypt = require("bcryptjs")

const dotenv = require('dotenv')
dotenv.config()

const graphQlSchema = require("./graphql/schema")
const graphQlResolvers = require("./graphql/resolvers")
const isAuth = require("./middleware/auth")
const User = require('./models/user')
const path = require('path')
const file = require("./flag.json");
const cookieparser = require('cookie-parser');

const app = express();
const port = process.env.PORT || 8000

app.use(bodyparser.json())
app.use(cookieparser())
const views_path = path.join(__dirname, "./views")
const static_path = path.join(__dirname, "./public")
app.use(express.static(static_path))
app.set("view engine", "hbs");
app.set("views", views_path);

app.use('/graphql', graphqlHTTP((req, res) => ({
  schema: graphQlSchema,
  rootValue: graphQlResolvers,
  context: {req,res},
  graphiql: true,
})))

console.log(process.env.DB_URL)
mongoose.connect(`${process.env.DB_URL}`).then(async () => {
  const adminExist = await User.find({ username: "admin" })
  // console.log(adminExist)
  if (!adminExist || adminExist.length == 0) {
    const hashedPassword = await bcrypt.hash(file.flag, 12)
    const user = new User({
      username: "admin",
      password: hashedPassword
    })
    await user.save()
  }
  app.listen(port, () => {
    console.log(`server is run on http://localhost:8000`)
  })
}).catch(err => {
  console.log(err)
})