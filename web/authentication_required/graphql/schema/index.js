const { buildSchema } = require("graphql")
const {makeExecutableSchema} = require('graphql-tools')
const {GraphQLJSON,GraphQLJSONObject} = require("graphql-type-json")

const typeDefs = buildSchema(`
scalar JSON
scalar JSONObject

type Event {
    _id: ID!
    title: String!
    description: String!
    price: Float!
    date: String!
    creator: User!
}
input EventInput {
    title: String!
    description: String!
    price: Float!
}

input UserInput{
    username: String!
    password: String!
}

type AuthData{
    userId: ID
    token: String
    tokenExpiration: Int
}

type User{
    _id: ID!
    username: String!
    password: String
    createdEvent: [Event!]
}



type RootQuery {
    events: [Event!]!
    login(username: String!,password: String!): AuthData!
}
type RootMutation {
    createEvent(eventInput: EventInput): Event
    createUser(userInput: UserInput): User
}
schema {
    query : RootQuery
    mutation : RootMutation
}
`)

const resolvers = {
    JSON: GraphQLJSON,
    JSONObject: GraphQLJSONObject
}

module.exports = makeExecutableSchema({typeDefs,resolvers});


