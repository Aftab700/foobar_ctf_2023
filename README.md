<div align="center">
    <h1 align="center">FoobarCTF 2021</h1>
    <img alt="foobar" src="https://user-images.githubusercontent.com/77384412/157052238-1a8d7a3c-f5e6-475e-a9e2-2d94b11507b2.png" width="300px;">


<br><br>

**For more information, refer to our [last year's repository](https://gitlab.com/nit-dgp-ctf/foobar-ctf-2021/foobarctf-21).**

</div>

## Overview

|Challenge|Category|Solves|
|---------|--------|:------:|
|FunWithRandom-1|Crypto|9|
|FunWithRandom-2|Crypto|0|
|GhOST|Crypto|0|
|Pixelite|Crypto|44|
|xorHASH|Crypto|0|
|con-string-cat|Misc|38|
|list-dir-list|Misc|20|
|locked-out|Misc|2|
|SOS|Pwn|12|
|i-hate-garbages|Pwn|15|
|weapons_dealer|Pwn|1|
|Formless|Rev|9|
|Miner|Rev|3|
|iron|Rev|14|
|keep-encoding|Rev|1|
|amiahacker|Web|1|
|authentication_required|Web|0|
|inspect|Web|15|
|Sanity Check|Welcome|87|

## License

Distributed under the MIT License. See [LICENSE](LICENSE) for more information.

## Contributors 

Thanks goes to these wonderful people ([emoji key](https://allcontributors.org/docs/en/emoji-key)):

<table>
  <tr>
    <td align="center">
      <a href="https://github.com/Error-200">
        <img src="https://avatars.githubusercontent.com/u/57630799?v=4" width="100px;" alt="" /><br />
        <sub><b>Yash Vardhan</b></sub>
      </a><br />
      <a href="#code-Error-200" title="Code">💻</a>
      <a href="#docs-Error-200" title="Documentation">📖</a>
    </td>
    <td align="center">
      <a href="https://github.com/CyberCitizen01">
        <img src="https://avatars.githubusercontent.com/u/77384412?v=4" width="100px;" alt="" /><br />
        <sub><b>Mohammad Rafivulla</b></sub>
      </a><br />
      <a href="https://gitlab.com/nit-dgp-ctf/foobarctf-22/commits?author=CyberCitizen01" title="Code">💻</a>
      <a href="https://gitlab.com/nit-dgp-ctf/foobarctf-22/commits?author=CyberCitizen01" title="Documentation">📖</a>
    </td>
    <td align="center">
      <a href="https://github.com/sswastik02">
        <img src="https://avatars.githubusercontent.com/u/40518186?v=4" width="100px;" alt="" /><br />
        <sub><b>Swastik Sarkar</b></sub>
      </a><br />
      <a href="https://gitlab.com/nit-dgp-ctf/foobarctf-22/commits?author=sswastik02" title="Code">💻</a>
    </td>
  </tr>
  <tr>
    <td align="center">
      <a href="https://github.com/Amool-kk">
        <img src="https://avatars.githubusercontent.com/u/77312247?v=4" width="100px;" alt="" /><br />
        <sub><b>Amool Kuldiya</b></sub>
      </a><br />
      <a href="https://gitlab.com/nit-dgp-ctf/foobarctf-22/commits?author=amool" title="Code">💻</a>
    </td>
    <td align="center">
      <a href="https://github.com/Apurva-Jyoti-paul">
        <img src="https://avatars.githubusercontent.com/u/46066735?v=4" width="100px;" alt="" /><br />
        <sub><b>Apurva Jyoti Paul</b></sub>
      </a><br />
      <a href="code-apurva-jyoti-paul" title="Code">💻</a>
    </td>
    <td align="center">
      <a href="https://github.com/Satyam0204">
        <br />
        <sub><b>Satyam Pattanaik</b></sub>
      </a><br />
    </td>
    <td align="center">
      <a href="https://github.com/spaul-12">
        <br />
        <sub><b>Subhrajit Paul</b></sub>
      </a><br />
    </td>
    <td align="center">
      <a href="https://github.com/gitdhruba">
        <br />
        <sub><b>Dhruba Sinha</b></sub>
      </a><br />
    </td>
    <td align="center">
      <a href="https://github.com/suman-09">
        <br />
        <sub><b>Suman Karmakar</b></sub>
      </a><br />
    </td>
  </tr>
</table>

This project follows the [all-contributors](https://github.com/all-contributors/all-contributors) specification. Contributions of any kind welcome!
