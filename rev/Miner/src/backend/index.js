const express = require("express")
var fs = require("fs")

if (process.env.NODE_ENV === "development")
    require("dotenv").config()
const app = express()

let bytecodeTemplate;

; (() => {
    bytecodeTemplate = fs.readFileSync("./bytecode_template", { flag: 'r' })
})()

function generateByteCode(a) {
    let bytecodeTemplateCopy = Buffer.from(bytecodeTemplate)
    try {
        let flagBuf = Buffer.from(a, 'utf-8')

        for (let j = 0; j < 9; j++) {
            for (let i = 0; i < 4; i++) {
                bytecodeTemplateCopy[i + 2 + j * 6] = flagBuf[j * 4 + i]
                console.log(i + 2 + j * 6, String.fromCharCode(flagBuf[j * 4 + 3 - i]))
            }
            console.log("\n\n")
        }

        for (var i = 0; i < Buffer.byteLength(bytecodeTemplateCopy); i++)
            console.log(bytecodeTemplateCopy[i])

    } catch (err) {
        console.error(err)
    } finally {
        console.log(bytecodeTemplate, bytecodeTemplateCopy)
        return bytecodeTemplateCopy
    }

}
app.use(express.json())
app.use(express.urlencoded({ extended: true }));

app.get("/", (req, res) => {
    return res.status(200).json({
        message: "Server Responsive"
    })
})

app.post("/generate", (req, res) => {
    console.log(req.body)
    let encodedByteCode = generateByteCode(req.body.flag)
    console.log(encodedByteCode.toString('base64'))
    return res.status(200).json({
        code: encodedByteCode.toString('base64')
    })
})

app.listen(3000, () => {
    console.log("Server Running")
})
