import base64 
import struct

a = b'BABHTFVHBAF7akF2BAJBX0NfBANiWVQzBARfQ29EBAUzX01pBAZOSW45BAd9AAAABAgAAAAABAt+sTcTBAw5/WJUAAALAAAMAAEMBAw9alR1AAEMBAu+un7BBAz/5T2eAAILAAIMBAs3EwAABAxVSlQzAAMLAAMMAAQFBAtsHCItAAQLBAszX01pBAwAAAAAAAULAAUMBAsJAAAABAxHSW45AAYLAAYMBAsDAAAABAx+AAAAAAcLAAcMAgABAgACAgADAgAEAgAF/w=='
d = base64.b64decode(a)

pc = 0

while pc < len(d)-2:
    print("%.2x : " % pc,end=' ')
    if(d[pc] == 0):
        print("XOR r%i, r%i\n" % (d[pc+1],d[pc+2]))
        pc+=3
        continue

    if(d[pc] == 1):
        print("AND r%i, r%i \n" % (d[pc+1],d[pc+2]))
        pc+=3
        continue

    if(d[pc] == 2):
        print("OR r%i, r%i \n" % (d[pc+1],d[pc+2]))
        pc+=3
        continue

    if(d[pc] == 3):
        print("MOV r%i, r%i \n" % (d[pc+1],d[pc+2]))
        pc+=3
        continue

    if(d[pc] == 4):
        print("MOVI r%i, 0x%.8x \n" % (d[pc+1],struct.unpack("I",d[pc+2:pc+2+4])[0]))
        pc+=6
        continue
    else :
        print("")
        pc+=1
        continue

