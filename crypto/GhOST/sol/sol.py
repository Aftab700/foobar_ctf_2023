from pwn import *

p = process(['python', 'src/__main__.py'], cwd='..')
trials = 0
p.sendlineafter(b'>', b'B'); trials += 1
p.sendlineafter(b':', b'00')
p.sendlineafter(b'>', b'B'); trials += 1
p.sendlineafter(b':', b'00')
z_ = p.recvuntil(b'\n').strip().decode()
z_ = int.from_bytes(bytes.fromhex(z_), 'big')
print(f'{z_ = }')

p.sendlineafter(b'>', b'F'); trials += 1
I = p.recvuntil(b'\n').strip().decode()
I = int.from_bytes(bytes.fromhex(I), 'big')
print(f'{I = }')

p.sendlineafter(b'>', b'E'); trials += 1
p.sendlineafter(b':', b'00')
ct1 = p.recvuntil(b'\n').strip().decode()
ct1 = int.from_bytes(bytes.fromhex(ct1), 'big')
print(f'{ct1 = }')
a = (ct1 >> 32) & 0xffffffff
zs = []
for i in range(16):
    pt = (z_ | (i<<14)) << 32
    p.sendlineafter(b'>', b'E'); trials += 1
    p.sendlineafter(b':', pt.to_bytes(8, 'big').hex().encode())
    ct2 = p.recvuntil(b'\n').strip().decode()
    ct2 = int.from_bytes(bytes.fromhex(ct2), 'big')
    # print(f'{i} {pt = } {ct2 = }')
    r = (ct2 >> 00) & 0xffffffff
    if(r == a):
        # print(pt >> 32)
        zs.append(pt >> 32)
print(f'{zs = }')
# z 3307278700
favourite_row = [0 for _ in range(16)]

i = 7 - I
for z in zs:
    for l in range(16):
        for m in range(16):
            x = l << (4*i)
            y = (z & ~(((0xf << ((4*i + 11) % 32)) & 0xffffffff) | ((0xf >> ((21 - 4*i) % 32)) & 0xffffffff))) | (((m << ((4*i + 11) % 32)) | (m >> ((21 - 4*i) % 32))) & 0xffffffff)
            print(f'{z = } {i = } {l = } {m = } {x = } {y = }')
            pt = (x << 32)
            p.sendlineafter(b'>', b'E'); trials += 1
            p.sendlineafter(b':', pt.to_bytes(8, 'big').hex().encode())
            ct1 = p.recvuntil(b'\n').strip().decode()
            ct1 = int.from_bytes(bytes.fromhex(ct1), 'big')
            a = (ct1 >> 32) & 0xffffffff
            pt = (y << 32) + x
            p.sendlineafter(b'>', b'E'); trials += 1
            p.sendlineafter(b':', pt.to_bytes(8, 'big').hex().encode())
            ct2 = p.recvuntil(b'\n').strip().decode()
            ct2 = int.from_bytes(bytes.fromhex(ct2), 'big')
            a_ = (ct2 >> 00) & 0xffffffff
            if(a == a_):
                print(l, m)
                favourite_row[l] = m
                break
        print()
    print()

print(favourite_row, trials)

import hashlib

h = hashlib.new('sha256')
h.update(bytes(favourite_row).hex().encode())
print(f'GLUG{{{h.hexdigest()}}}')
